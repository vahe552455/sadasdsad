<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserChange extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|unique:users,email,'.$this->request->get('id'),
            'name' => 'required|string|',
            'country_id' =>'required|numeric'
        ];
    }
}
