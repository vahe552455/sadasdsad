<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //bind user repo
        $this->app->bind(
            'App\Contracts\User\UserInterface',
            'App\Repositories\User\UserRepo'
        );
        //bind country repo

        $this->app->bind(
            'App\Contracts\Country\CountryInterface',
            'App\Repositories\Country\CountryRepo'
        );

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
