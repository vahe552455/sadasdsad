<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('register', 'AuthController@register');
Route::get('countries', 'AuthController@countries');
Route::post('login', 'AuthController@login');
Route::get('refresh', 'AuthController@refresh');
Route::group(['middleware' => 'auth:api'], function() {
    Route::post('logout', 'AuthController@logout');

    Route::group(['middleware' => 'isAdmin','namespace'=>'Api\Admin\Users','prefix'=>'admin'], function () {
       Route::get('users','UserController@index');
       Route::post('users','UserController@store');
       Route::get('users/{id}','UserController@edit');
       Route::put('users/{id}','UserController@update');
       Route::delete('users/{id}','UserController@destroy');
    });
    
    Route::group(['middleware' => 'auth:api'], function () {
        Route::get('get-user/', 'AuthController@getUser');
    });
});
